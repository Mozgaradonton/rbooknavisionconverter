import mozga.excel.util.RBookNavisionConverter;


public class ConvertionExcelFiles {
	public static void main(String[] args) throws Exception {
		RBookNavisionConverter converter;
		if (args.length == 2) {
			converter= RBookNavisionConverter.getConverter(args[0], args[1]); 
		} else {
			converter= RBookNavisionConverter.getConverter(); 
		}
		converter.doConversion();
	}
	
}
