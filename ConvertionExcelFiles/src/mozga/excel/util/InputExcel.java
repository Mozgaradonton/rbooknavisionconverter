package mozga.excel.util;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class InputExcel {
	private Workbook inputWorkbook;
	private Sheet worksheet;
	
	public static final String DATE = "����";
	public static final String SOURCE = "��������";
	public static final String PAYMENT_PLACE = "����� �������";
	public static final String ARTICLE = "������";
	public static final String CONTRACT_NUMBER = "����� ��������";
	public static final String COMMENT = "����������";
	public static final String BANK = "����";
	public static final String AMOUNT = "�����";
	public static final String AMOUNT_EURO = "#";
	
	private static final String[] columns = new String[] {DATE, SOURCE, PAYMENT_PLACE,
							ARTICLE, AMOUNT, AMOUNT_EURO, CONTRACT_NUMBER, COMMENT, BANK};
	
	private Map<String, Integer> columnsMap = new HashMap<String, Integer>();
	
	private int firstRow; 
	private int lastRow;
	private int currentRow;
	
	InputExcel(String fileName, String sheetName) throws Exception {
		inputWorkbook = WorkbookFactory.create(new FileInputStream(fileName));
		worksheet = inputWorkbook.getSheet(sheetName);
		if (worksheet == null) {
			worksheet = inputWorkbook.getSheet("����1");
		}
		
		populateColumnsMap();
		firstRow = getFirstRowIndex();
		currentRow = firstRow;
		lastRow = getLastRowIndex();
	}
	
	public int getColumn (String colName) {
		return columnsMap.get(colName);
	}
	
	private void populateColumnsMap () throws Exception {
		for (String colName: columns) {
			int colNum = getColumnNum(colName);
			if (colNum == -1) {
				throw new Exception("The column " + colName + " is not found in the file");
			}
			columnsMap.put(colName, colNum);        
		}
	}
	
	private int getColumnNum(String colName) throws Exception {
		for (int rowNum = 0; rowNum < 3; rowNum++) {
			Row row = worksheet.getRow(rowNum);
			
			if (row != null) {
				int lastColumn = Math.max(row.getLastCellNum(), 20);
				
				for (int cn = 0; cn < lastColumn; cn++) {
					Cell cell = row.getCell(cn, Row.CREATE_NULL_AS_BLANK);
					if (cell != null) {
						if (cell.getCellType() == Cell.CELL_TYPE_STRING && cell.getStringCellValue().toUpperCase().trim().contains(colName)) {
							return cell.getColumnIndex();
						}
					}
				}
			}
			
		}
		return -1;
	}	
	
	private int getFirstRowIndex() {
		for (int rowNum = 0; rowNum < 100; rowNum++) {
			Row row = worksheet.getRow(rowNum); 
			if (row != null) {
				Cell cell = row.getCell(getColumn(DATE), Row.CREATE_NULL_AS_BLANK);
				if (cell.toString().matches("\\d\\d.+")) {
					return cell.getRowIndex();
				}
			}
		}
		return 0;
	}
	
	private int getLastRowIndex() {
		int rowNum = worksheet.getLastRowNum();
		
		while(true) {
		
			Row row = worksheet.getRow(rowNum); 
			if (row != null) {
				Cell cell = row.getCell(getColumn(DATE), Row.CREATE_NULL_AS_BLANK);
				if (cell.toString().matches("\\d\\d.+")) {
					return cell.getRowIndex();
				}
			}
			rowNum--;
		}
	}
	
	public Row getNextRow() {
		return worksheet.getRow(currentRow++);
	}
	
	public boolean hasNextRow() {
		return currentRow <= lastRow;
	}
	
	
}
