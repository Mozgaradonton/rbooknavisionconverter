package mozga.excel.util;

import mozga.excel.util.resources.BankAccountsList;
import mozga.excel.util.resources.Projects;
import mozga.excel.util.resources.Vendors;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class RBookNavisionConverter {
	private static Row inputRow;
	private static InputExcel in;
	private static OutputExcel out;
	private static BankAccountsList bankAccountsList;
	private static Projects projects;
	private static Vendors vendors;
	
	public static RBookNavisionConverter getConverter() throws Exception {
		return new RBookNavisionConverter("inputFile", "outputFile");
	}

	public static RBookNavisionConverter getConverter(String inputFile, String outputFile) throws Exception {
		return new RBookNavisionConverter(inputFile, outputFile);
	}
	
	private RBookNavisionConverter(String inputFileName, String outputFileName) throws Exception {
		in = new InputExcel(inputFileName + ".xlsx", "Sheet1");
		
		out = new OutputExcel(outputFileName + ".xlsx", "Sheet1");
		
		bankAccountsList = new BankAccountsList("./resources/BankAccountsList" + ".xlsx", "Sheet1");
		projects = new Projects("./resources/Projects" + ".xlsx", "Sheet1");
		vendors = new Vendors("./resources/Vendors" + ".xlsx", "Sheet1");
	}
	
	public void doConversion() throws Exception {
		while(in.hasNextRow()) {
			try {
				inputRow = in.getNextRow();
				out.setNextRow();
				
				convertDate();
				
				convertAmountAndCurrency();
	
				convertCode();
				
				convertSupplierNavCode();
				
				convertInvoiceNDD();
				
				convertPrimechanie();
				
				convertAccountCode();
				
				convertNewProjectNumber();
				
				upgradeSupplierNavCode();
			} catch (Exception e) {
				int row = inputRow.getRowNum() + 1;
				String method = e.getStackTrace()[0].getMethodName();
				out.setStringValue(OutputExcel.DATE, "!!!ERROR in row " + row + ". method " + method);
			}
			
		}
		out.setAutoSize();
		
		out.saveFile();

		System.out.println("Conversion is done!");
	}
	
	@SuppressWarnings("deprecation")
	private void convertDate() {
		Cell cellDate = inputRow.getCell(in.getColumn(InputExcel.DATE), Row.CREATE_NULL_AS_BLANK);
		String date = cellDate.getDateCellValue().toLocaleString().split(" ")[0];
		out.setStringValue(OutputExcel.DATE, date);
	}
	
	private void convertAmountAndCurrency() {
		double amount = 0.0;
		double amountReceived = 0.0;
		String currency = "";
		int amountEuroColumn = in.getColumn(InputExcel.AMOUNT_EURO);
		for (int column = amountEuroColumn; column < amountEuroColumn + 4; column++) {
			Cell amountCell = inputRow.getCell(column, Row.CREATE_NULL_AS_BLANK);
			
			if (amountCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
				currency = getCurrencyText(amountEuroColumn, column );
				if (amountCell.getNumericCellValue() < 0) {
					amount = amountCell.getNumericCellValue();
					break;
				} else {
					amountReceived = amountCell.getNumericCellValue();
					break;
				}
			}
        }
		
		out.setDoubleValue(OutputExcel.AMOUNT, -amount);
		out.setDoubleValue(OutputExcel.AMOUNT_RECEIVED, amountReceived);
		out.setStringValue(OutputExcel.CURRENCY, currency);
		out.setStringValue(OutputExcel.COUNTERPARTY_TYPE, amountReceived > 0 ? "Client" : "Vendor");
	}
	
	private void convertCode() {
		Cell cell = inputRow.getCell(in.getColumn(InputExcel.COMMENT), Row.CREATE_NULL_AS_BLANK);
		Cell article = inputRow.getCell(in.getColumn(InputExcel.ARTICLE), Row.CREATE_NULL_AS_BLANK);
		if (article.getStringCellValue().toLowerCase().contains("��������")) {
			out.setStringValue(OutputExcel.CODE, "07.12");
			out.setStringValue(OutputExcel.SUPPLIER_NAV_CODE, "Bank");
		} else {
			String text = cell.getStringCellValue();
			String code = getCode(text);
			if (code.matches("\\d\\..+")) {
				code = "0" + code;
			}
			out.setStringValue(OutputExcel.CODE, code.split("/")[0]);
		}
	}

	private void convertSupplierNavCode() {
		Cell cell = inputRow.getCell(in.getColumn(InputExcel.COMMENT), Row.CREATE_NULL_AS_BLANK);
		String text = cell.getStringCellValue();
		String code = out.getCell(OutputExcel.CODE).getStringCellValue();
		if (!getCode(text).isEmpty() && !(code.equals("07.12"))) {
			String supplierNavCode = text.split(";")[ 1 ].trim();
			out.setStringValue(OutputExcel.SUPPLIER_NAV_CODE, supplierNavCode);
		}
	}
	
	private void convertInvoiceNDD () throws Exception {
		Cell cell = inputRow.getCell(in.getColumn(InputExcel.COMMENT), Row.CREATE_NULL_AS_BLANK);
		String text = cell.getStringCellValue();
		if (!getCode(text).isEmpty()) {
			String invoiceNDD = text.split(";")[2];
			invoiceNDD = invoiceNDD.split("for")[ 0 ];
			out.setStringValue(OutputExcel.INVOICE_N_DD, invoiceNDD.trim());
		}
	}
	
	private void convertPrimechanie() {
		Cell cell = inputRow.getCell(in.getColumn(InputExcel.COMMENT), Row.CREATE_NULL_AS_BLANK);
		String text = cell.getStringCellValue();
		out.setStringValue(OutputExcel.PRIMICHANIE, text);
	}
	
	private void convertAccountCode() throws Exception {
		Cell cell = inputRow.getCell(in.getColumn(InputExcel.BANK), Row.CREATE_NULL_AS_BLANK);
		String code = cell.getStringCellValue().trim();
		String currency = out.getCell(OutputExcel.CURRENCY).getStringCellValue();
		String accountCode = bankAccountsList.getMatching(code, currency);
		out.setStringValue(OutputExcel.ACCOUNT_CODE, accountCode);
	}
	
	private void upgradeSupplierNavCode() {
		Cell cell = out.getCell(OutputExcel.SUPPLIER_NAV_CODE);
		if (cell != null) {
			String currentNavCode = cell.getStringCellValue();
			if (!currentNavCode.isEmpty() && !currentNavCode.equals("Bank")) {
				String newNavCode = vendors.getNewNavCode(currentNavCode);
				out.setStringValue(OutputExcel.SUPPLIER_NAV_CODE, newNavCode);
			}
		}
	}
	
	private void convertNewProjectNumber() {
		Cell cell = inputRow.getCell(in.getColumn(InputExcel.CONTRACT_NUMBER), Row.CREATE_NULL_AS_BLANK);
		String dogovorNumber = cell.getStringCellValue().trim();
		if (!dogovorNumber.isEmpty()) {
			String nums = projects.getProjectNumbers(dogovorNumber);
			if (nums.length() > 0) {
				out.setStringValue(OutputExcel.NEW_PROJECT_NUMBER, nums.split(";")[0]);
				out.setStringValue(OutputExcel.NEW_SUB_PROJECT_NUMBER, nums.split(";")[1]);
			}
		}
	}
	

	private String getCurrencyText(int amountEuroColumn, int column) {
		String currency = "";
		
		if        (amountEuroColumn == column) {
			currency = "EUR";
		} else if ((amountEuroColumn + 1) == column) {
			currency = "USD";
		} else if ((amountEuroColumn + 2) == column) {
			currency = "RUB";
		} else if ((amountEuroColumn + 3) == column) {
			currency = "GBP";
		}
		
		return currency;
	}
	

	
	private String getCode(String text) {
		String[] tokens = text.split("[ ,]");
		String result = "";
		for (String token: tokens) {
			if (token.matches("\\d{1,2}\\.\\d{1,2}(/\\d{1,2})*")) {
				result = token;
				break;
			}
		}
		return result.trim();
	}
	

	
	
}
