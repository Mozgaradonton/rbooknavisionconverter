package mozga.excel.util;

import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class OutputExcel {
	public static final String DATE = "Date"; 
	public static final String AMOUNT = "Amount"; 
	public static final String AMOUNT_RECEIVED = "Amount Received"; 
	public static final String NEW_PROJECT_NUMBER = "New Project Number"; 
	public static final String NEW_SUB_PROJECT_NUMBER = "New Sub-project Number"; 
	public static final String CURRENCY = "Currency"; 
	public static final String CODE = "Code";
	public static final String COUNTERPARTY_TYPE = "Counterparty type";
	public static final String SUPPLIER_NAV_CODE = "Supplier_NAV_Code";
	public static final String INVOICE_N_DD = "Invoice N dd";
	public static final String PRIMICHANIE = "����������";
	public static final String ACCOUNT_CODE = "Account Code";
	
	private String outputFileName;
	private Workbook outputWorkbook;
	private Sheet worksheet;
	private final String[] columns = new String[] {DATE, AMOUNT, AMOUNT_RECEIVED, CURRENCY,
			NEW_PROJECT_NUMBER, NEW_SUB_PROJECT_NUMBER, CODE, COUNTERPARTY_TYPE,
			SUPPLIER_NAV_CODE, INVOICE_N_DD, PRIMICHANIE, ACCOUNT_CODE};
	private int currentRowIndex = 1;
	private Row currentRow;
	
	OutputExcel(String fileName, String sheetName) throws Exception {
		outputWorkbook = new XSSFWorkbook();
		outputFileName = fileName;
		worksheet = outputWorkbook.createSheet(sheetName);
		renderHeader();
	}
	
	public Cell getCell(String colName) {
		return currentRow.getCell(getColNum(colName));
	}
	
	private int getColNum(String colName) {
		for (int i = 0; i < columns.length; i++) {
			if (columns[ i ].equals(colName)) {
				return i;
			}
		}
		return -1;
	}
	
	public void setStringValue(String columnName, String value) {
		Cell cell = currentRow.createCell(getColNum(columnName));
		cell.setCellValue(value);
	}
	
	public void setDoubleValue(String columnName, double amount) {
		Cell cell = currentRow.createCell(getColNum(columnName));
		cell.setCellValue(amount);
	}
	
	public void setNextRow() {
		currentRow = worksheet.createRow(currentRowIndex++);
	}
	
	private void renderHeader() {
		Row row = worksheet.createRow(0);
		CellStyle style = outputWorkbook.createCellStyle();
		Font font = outputWorkbook.createFont();
		font.setColor(IndexedColors.RED.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style.setFont(font);
		for (int i = 0; i < columns.length; i++) {
			row.createCell(i).setCellValue(columns[i]);
			row.getCell(i).setCellStyle(style);
		}
	}
	
	public void setAutoSize() {
		for(int i = 0; i < columns.length; i++) {
			worksheet.autoSizeColumn(i);
		}
	}
	
	public void saveFile() throws Exception {
	    FileOutputStream fileOut = new FileOutputStream(outputFileName);
	    outputWorkbook.write(fileOut);
	    fileOut.close();
	}
}
