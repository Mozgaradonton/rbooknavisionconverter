package mozga.excel.util.resources;


public class BankAccountsList extends MatchTable{
	private int rbookColumn;
	private int navisionCodeColumn;
	private int currencyColumn;
	private int firstRow;
	
	
	public BankAccountsList(String fileName, String sheetName) throws Exception {
		super(fileName, sheetName);

		rbookColumn = getCellWithText("rbook").getColumnIndex();
		navisionCodeColumn = getCellWithText("NAVISION CODE").getColumnIndex();
		currencyColumn = getCellWithText("currency").getColumnIndex();
		firstRow = getCellWithText("rbook").getRowIndex() + 1;
	}
	
	public String getMatching(String code, String currency) {
		if (!code.isEmpty()) {
			for (int index = firstRow; index < worksheet.getLastRowNum(); index++) {
				
				String rbook = worksheet.getRow(index).getCell(rbookColumn).getStringCellValue();
				String cur = worksheet.getRow(index).getCell(currencyColumn).getStringCellValue().substring(0, 3);
				if (code.equalsIgnoreCase(rbook) && currency.equalsIgnoreCase(cur)) {
					return worksheet.getRow(index).getCell(navisionCodeColumn).getStringCellValue();
				}
			}
		}
		return "";
	}
	

	
	
}
