package mozga.excel.util.resources;


public class Projects extends MatchTable{	
	private int rbookNumberColumn;
	private int newProjectNumberColumn;
	private int newSubProjectNumber;
	private int firstRow;
	
	
	public Projects(String fileName, String sheetName) throws Exception {
		super(fileName, sheetName);
		
		rbookNumberColumn = getCellWithText("rbook number").getColumnIndex();
		newProjectNumberColumn = getCellWithText("New PROJECT Number").getColumnIndex();
		newSubProjectNumber = getCellWithText("New SUB-PROJECT Number").getColumnIndex();
		firstRow = getCellWithText("rbook number").getRowIndex() + 1;
	}
	
	public String getProjectNumbers(String dogNumber) {
		for (int i = firstRow; i < worksheet.getLastRowNum(); i++) {
			String rbook = worksheet.getRow(i).getCell(rbookNumberColumn).getStringCellValue();
			if (dogNumber.equalsIgnoreCase(rbook)) {
				String newProject = worksheet.getRow(i).getCell(newProjectNumberColumn).getStringCellValue();
				String newSubProject = worksheet.getRow(i).getCell(newSubProjectNumber).getStringCellValue();
				return newProject + ";" + newSubProject;
			}
		}
		
		return "";
	}
}
