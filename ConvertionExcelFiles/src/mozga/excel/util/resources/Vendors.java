package mozga.excel.util.resources;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class Vendors extends MatchTable {	
	private int rbookColumn;
	private int numberColumn;
	private int firstRow;
	
	public Vendors(String fileName, String sheetName) throws Exception {
		super(fileName, sheetName);
		
		rbookColumn = getCellWithText("Name R-Book").getColumnIndex();
		numberColumn = getCellWithText("No.").getColumnIndex();
		firstRow = getCellWithText("Name R-Book").getRowIndex() + 1;
	}
	
	public String getNewNavCode(String currentCode) {
		for (int i = firstRow; i < worksheet.getLastRowNum(); i++) {
			Row row = worksheet.getRow(i);
			if (row != null) {
				Cell cell = worksheet.getRow(i).getCell(rbookColumn);
				if (cell != null && cell.getCellType() == Cell.CELL_TYPE_STRING) {
					String rbook = cell.getStringCellValue();
					if (currentCode.equalsIgnoreCase(rbook)) {
						return worksheet.getRow(i).getCell(numberColumn).getStringCellValue();
					}
				}
			}
		}
		
		return "";
	}	
}
