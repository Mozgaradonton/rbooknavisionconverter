package mozga.excel.util.resources;

import java.io.FileInputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public abstract class MatchTable {
	Workbook inputWorkbook;
	Sheet worksheet;
	
	MatchTable(String fileName, String sheetName) throws Exception {
		inputWorkbook = WorkbookFactory.create(new FileInputStream(fileName));
		worksheet = inputWorkbook.getSheet(sheetName);
	}
	
	Cell getCellWithText(String text) {
		text = text.trim();
		for (Row row: worksheet) {
			for (Cell cell: row) {
				if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
					String cellText = cell.getStringCellValue().trim();
					if (cellText.equalsIgnoreCase(text)) {
						return cell;
					}
				}
			}
		}
		return null;
	}
}
